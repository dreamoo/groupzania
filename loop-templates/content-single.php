<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(array("card")); ?> id="post-<?php the_ID(); ?>" style="max-width:500px;margin:0 auto;">

	<header class="card-header">

		<?php the_title( '<h3>', '</h3>' ); ?>

	</header><!-- .entry-header -->
	<div style="padding:15px;text-align:center">
		<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
	</div>
	<div class="card-body" style="text-align:center">

		<?php the_content(); ?>
		<a href="#" onclick="javascript:return false;" class="btn btn-primary">Join Group</a>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
