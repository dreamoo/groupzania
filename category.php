<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<div class="wrapper" id="index-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">
		<h1 class="entry-title"><?php single_cat_title();?></h1>
		<main class="site-main home-items" id="main">
			<div class="row">
			  <?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
				<div class="col-12 col-md-3 col-lg-3">
					<article <?php post_class(array('item')); ?> id="post-<?php the_ID(); ?>">
						<?php if(has_post_thumbnail()):?>
							<?php echo get_the_post_thumbnail( $post->ID, 'large' ,array('class'=>'bd-placeholder-img item-img')); ?>
						<?php else:?>
							<img src="<?php echo get_template_directory_uri();?>/images/placeholder.png" class="bd-placeholder-img item-img" alt="GroupZania" />
						<?php endif;?>
						<header class="entry-header">

							<?php
							the_title(
								sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
								'</a></h3>'
							);
							?>

						</header><!-- .entry-header -->
						<br />
						<!--div class="desc">
							<?php the_excerpt(); ?>
						</div><!-- .entry-content -->
						<a class="btn btn-primary" href="<?php echo esc_url( get_permalink() );?>" role="button">Join Group »</a>
						<br /><br />
					</article><!-- #post-## -->
				</div>
				<?php endwhile; ?>
				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>
			</div>
		</main>

	</div>

</div>

<?php get_footer(); ?>
